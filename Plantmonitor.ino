#include <Arduino.h>
#include <DFRobotDFPlayerMini.h>

// pins
const int soilPin = A0;
const int motionPin = 15;

// calibration values
const int airValue = 800;
const int waterValue = 320;

// limits (TODO: make them settable from outside - homeassistant?)
const int minimumMoisture = 60; // When below this percentage => Alarm!
const int messageInterval = 10; // Seconds between Sound alerts

DFRobotDFPlayerMini player;

// use calibration values to determine the percentage
int getMoisturePercent() {
  int sensorVal = analogRead(soilPin);
  int percent = map(sensorVal, airValue, waterValue, 0, 100);
  return constrain(percent, 0, 100);
}

void setup() {
  Serial.begin(115200);
  Serial.println("Hello!");
  Serial1.begin(9600);

  pinMode(motionPin, INPUT);

  Serial.println("ESP8266 is ready!");

  if (!player.begin(Serial1)) {
    Serial.println("Failed initializing DFPlayerMini!");
    return;
  }
  Serial.println("DFPlayerMini initialized.");
  player.volume(20);
}

// time of latest sound alert
long messageTime = 0;

void loop() {
  long currentTime = millis() / 1000;
  Serial.print("Time:");
  Serial.println(currentTime);

  int moisture = getMoisturePercent();
  Serial.print("Moisture Percent:");
  Serial.println(moisture);

  bool motion = (digitalRead(motionPin) == HIGH);
  if (moisture < minimumMoisture) {
    if (motion) {
      if (currentTime - messageTime > messageInterval) {
        messageTime = currentTime;
        Serial.println("Too dry and motion detected!");
        player.next();
      } else {
        Serial.println("Too dry, but already alerted.");
      }
    } else {
      Serial.println("Too dry, but nobody here :(");
    }
  }
  delay(1000);
}
