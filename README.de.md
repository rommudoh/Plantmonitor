# Sprechender Planzenmonitor

![Plantmonitor.jpg](Plantmonitor.jpg)

Dies ist mein erstes richtiges Projekt mit ESP8266 Mikrocontrollern. Inspiriert wurde ich durch das Video "Fluchende Pflanze bauen!" von achNina: <https://youtu.be/W30yRRa995E>

## Motivation

Ich habe mich vor Kurzem ernsthaft mit dem Programmieren von Mikrocontrollern auseinander gesetzt und mir ein ESP32-S3 Starter Set geholt. Es hat mir Spaß gemacht und ich wollte etwas praktisches aus dem erlernten umsetzen. Unsere Pflanzen im Haus werden oft zu spät und dann zu stark gegossen, was ihnen sicher nicht gut tut, daher dachte ich, das Projekt von achNina kann ich sicher gut selbst gebrauchen.

## Ziel des Projekts

Wie bei der Vorlage wollte ich einen Bodenfeuchte-Sensor mit einem Lautsprecher und Bewegungsmelder verknüpfen, so dass ein Sound abgespielt wird, wenn jemand in der Nähe ist und die Erde zu trocken ist.

Im Gegensatz zu achNina wollte ich aber einen ESP8266 Controller einsetzen, da ich als nächsten Schritt die Integration in Homeassistant mittels ESPhome anstrebe. Dieses Projekt ist also nur ein Zwischenschritt zum ersten praktischen Umsetzen einer Idee.

## Benutzte Bauteile und Kosten

Bauteile, die ich für dieses Projekt benutzt habe:

* FREENOVE ESP8266 Development Board ESP-12S
* DFPlayer Mini MP3 Player for Arduino
* Micro-SD Karte (32GB)
* PIR Bewegungsmelder HC-SR505
* Kapazitativer Bodenfeuchte-Sensor
* Mini Breadboard (80 x 54 mm, 400 Points)
* Kunststoffgehäuse (100 x 60 x 25 mm)
* 2 Stück 1kΩ Widerstände
* Steckkabel, Drahtbrücken, 2er Pin-Leiste
* USB-Netzteil mit Micro-USB Kabel

Kosten für die Komponenten, außer USB-Netzteil und die Kabel/Drähte belaufen sich auf etwa 28€. Die meisten Komponenten gibt es in mehrfach-Packungen günstiger.

Hier eine bildliche Darstellung:

![Plantmonitor_components.jpg](Plantmonitor_components.jpg)

## Einschränkungen

Der ESP8266 besitzt nur einen Analog-Eingang, daher kann damit nur ein Bodenfeuchtesensor betrieben werden, sofern man nicht multiplexen will oder einen weiteren Chip benutzen möchte, siehe auch hier: <https://www.instructables.com/ESP8266-with-Multiple-Analog-Sensors>. Eine andere Möglichkeit wäre, einen ESP32 zu nutzen.

Im Vergleich zu einem Arduino Uno bzw. ATmega328P als Controller ist der Stromverbrauch sicher wesentlich höher und da ich keinen Deep Sleep eingebaut habe ist ein Akku-Betrieb sicher möglich, dürfte aber recht schnell geleert sein. Dies ist nur mein Eindruck, um sicher zu gehen müsste ich es genauer Nachmessen.

## Aufbau

### Schaltungs-Diagramm

![Plantmonitor_schematic.jpg](Plantmonitor_schematic.jpg)

### Verkabelung Steckbrett

Bei der Verkabelung habe ich Drahtbrücken benutzt, da es dann kompakter ist und meiner Meinung nach besser aussieht. Ein paar der Drähte laufen unterhalb der Platinen, deshalb hier ohne die Bauteile:

![Plantmonitor_breadboard_cables.jpg](Plantmonitor_breadboard_cables.jpg)

Und hier mit den Bauteilen und in der Box:

![Plantmonitor_assembled.jpg](Plantmonitor_assembled.jpg)

### Gehäuse Modifikation

![Plantmonitor_box.jpg](Plantmonitor_box.jpg)

Die Box hat genau die richtige Größe für das kleine Steckbrett. Ich musste lediglich die Verbindungs-Noppen abtrennen. Da sie genau reinpasst, musste ich sie auch nicht am Boden festkleben.

In die Box ließ sich sehr gut mit einem Holzbohrer Löcher bohren.

Für die Kabel nach Außen (für Lautsprecher, Bodenfeuchte-Sensor und Stromversorgung) habe ich zwei 10mm Löcher nebeneinander gebohrt und mit einer Raspel geglättet.

Als seitliches Loch für den Bewegungssensor waren 10mm perfekt.

Hier ein Bild, wie genau das Steckbrett mit den Komponenten in die Box passt:

![Plantmonitor_closeup_box.jpg](Plantmonitor_closeup_box.jpg)

## Kalibrierung

Um die Grenzwerte des Bodenfeuchte-Sensors zu ermitteln, habe ich ein einfaches Testprogramm auf den Controller geladen und mir die Werte auf der seriellen Ausgabe angesehen, wenn der Sensor trocken an der Luft und in Wasser getaucht ist.

Dies ist eine sehr einfache und ungenaue Kalibrierung. Für eine Anleitung, wie es genauer geht, siehe hier: <https://makersportal.com/blog/2020/5/26/capacitive-soil-moisture-calibration-with-arduino>

## Programmierung

Bei der Programmierung habe ich mich an der Vorgabe orientiert. Allerdings mag ich keine zusätzlichen Delays, daher habe ich das Auslösen des Sounds anhand eines Zeitvergleichs gelöst. Außerdem berechne ich die Feuchtigkeit anhand der Kalibrierungswerte in Prozent.

Den Parameter für Mindestfeuchte habe ich geschätzt und möchte ich später von außen vorgeben können.

Benötigte Bibliotheken:

* DFRobot DFPlayer Mini

Ansonsten wird noch die bei ESP8266 enthaltene SoftwareSerial-Schnittstelle zur Kommunikation mit dem DFPlayer benutzt, da ich sie Standard serielle Schnittstelle für Statusinformationen benutze. Ansonsten hätte ich auch jeweils die RX- mit den TX-Pins verbinden und darauf verzichten können.

### Arduino IDE Einstellungen 

Als Board sollte im Untermenü ESP8266 der Eintrag "NodeMCU 1.0 (ESP-12E Module)" ausgewählt werden. Als Schnittstelle muss die entsprechende Serielle Schnittstelle ausgewählt werden. Bei mir war es "/dev/ttyUSB0".

## Verbesserungen

### Kein SoftwareSerial benutzen

Nachdem ich mir den ESP8266 nochmal genauer angesehen habe, habe ich festgestellt, dass der Controller einen zweiten seriellen Port hat, der allerdings nur senden kann (TX1, Pin D2).

Dadurch kann ich auf die SoftwareSerial-Bibliothek verzichten und den Controller entlasten.

Hier der modifizierte Schaltplan:

![Plantmonitor_schematic_v2.jpg](Plantmonitor_schematic_v2.jpg)

Und hier die aktualisierte Breadboard-Verkabelung:

![Plantmonitor_breadboard_cables_v2.jpg](Plantmonitor_breadboard_cables_v2.jpg)

Den Quellcode habe ich mit diesem Commit ebenfalls angepasst.

## ESPHome-Integration

Die Schaltung wird beibehalten, die Programmierung erfolgt aber über ESPHome. Ich habe hierfür das Projekt ein zweites Mal zusammengebastelt, damit der erste Aufbau meine Pflanze überwachen kann, bis ich es fertig in ESPHome eingebunden habe.

Zuerst wird in ESPHome ein neues Device angelegt. Als "Device Type" wird "ESP8266" ausgewählt, der Rest bleibt auf Standardeinstellungen.

Bevor die Firmware auf den Controller geflasht wird, muss die Konfiguration angepasst werden. Dafür wird "Edit" bei dem neu angelegten Gerät unter ESPHome ausgewählt.

Der Board-Typ muss passend zum Mikrocontroller ausgewählt werden. Bei mir ist es "nodemcuv2":

![esphome_device_type.png](esphome_device_type.png)

### Sensoren

#### Bodenfeuchte-Sensor

Unten wird für die Kalibrierung eine einfache Definition für den Bodenfeuchte-Sensor angehängt:

```yaml
sensor:
  - platform: adc
    pin: A0
    name: "Soil Moisture"
    id: moisture1
    accuracy_decimals: 4
    update_interval: 1s
    unit_of_measurement: "v"
    icon: "mdi:water-percent"
```

Danach kann die Firmware auf den Mikrocontroller installiert und der Output angesehen werden. Es sollte in etwa so aussehen:

```
[20:55:57][D][sensor:126]: 'Soil Moisture': Sending state 0.77832 v with 4 decimals of accuracy
[20:56:06][D][sensor:126]: 'Soil Moisture': Sending state 0.77832 v with 4 decimals of accuracy
```

Zum Ermitteln der Grenzwerte sollte der Wert des Sensors trocken an der Luft, sowie in Wasser getaucht genommen werden. Bei mir sind die Werte wie folgt:

| Zustand | Wert      |
|---------|-----------|
| Luft    | 0.78809 v |
| Wasser  | 0.30566 v |

Dies können wir nutzen, um den Wert in Prozent auszugeben. Dafür wird die Konfiguration des Sensors wie folgt ersetzt:

```yaml
sensor:
  - platform: adc
    pin: A0
    name: "Soil Moisture"
    id: moisture1
    accuracy_decimals: 1
    update_interval: 1s
    unit_of_measurement: "%"
    icon: "mdi:water-percent"
    filters:
    - median:
        window_size: 20
        send_every: 10
        send_first_at: 5
    - calibrate_linear:
        - 0.788 -> 0.00
        - 0.300 -> 100.00
    - lambda: |
        if (x < 0) return 0;
        else if (x > 100) return 100;
        else return (x);
```

#### Bewegungs-Sensor

Der Bewegungs-Sensor ist relativ einfach hinzuzufügen. Am Ende der Konfiguration wird folgendes angehängt:

```yaml
binary_sensor:
  - platform: gpio
    pin: 15
    name: "PIR Sensor"
    id: motion1
    device_class: motion
```

Sobald es auf den Mikrocontroller installiert wurde (jetzt kann ich das auch über Wlan machen, statt per USB zu verbinden), sollte in den Logs etwas in der Art ausgegeben werden, wenn man ein wenig vor dem Sensor winkt:

```
[21:05:34][D][sensor:126]: 'Soil Moisture': Sending state 2.58388 % with 4 decimals of accuracy
[21:05:38][D][binary_sensor:036]: 'PIR Sensor': Sending state ON
[21:05:44][D][sensor:126]: 'Soil Moisture': Sending state 2.58388 % with 4 decimals of accuracy
[21:05:51][D][binary_sensor:036]: 'PIR Sensor': Sending state OFF
[21:05:53][D][binary_sensor:036]: 'PIR Sensor': Sending state ON
[21:05:54][D][sensor:126]: 'Soil Moisture': Sending state 2.58388 % with 4 decimals of accuracy
[21:06:02][D][binary_sensor:036]: 'PIR Sensor': Sending state OFF
```

### DFPlayer Mini

ESPHome-Doku: <https://esphome.io/components/dfplayer.html>

Für mich interessant ist diese Aktion, die ich in einem Trigger ausführen möchte:

```yaml
on_...:
  then:
    - dfplayer.play_next:
```

Aber zuerst die Konfiguration des DFPlayers, incl. der UART-Schnittstelle:

```yaml
uart:
  - tx_pin: 2
    baud_rate: 9600
    id: uart_dfplayer

dfplayer:
  uart_id: uart_dfplayer
```

Für das Setzen der Lautstärke füge ich einen `on_boot` Trigger am Anfang der Konfiguration hinzu:

```yaml
esphome:
  # ...
  on_boot:
    then:
      - dfplayer.set_volume: 20
```

Zum Testen füge ich erstmal einen einfachen Service unter `api` hinzu:

```yaml
api:
  # ...
  services:
  - service: dfplayer_next
    then:
      - dfplayer.play_next:
```

Nach dem Upload der aktualisierten Firmware taucht der Service dann in Homeassistant auf und kann unter "Entwicklerwerkzeuge" von Hand ausgelöst werden:

![devtools_service_dfplayer_de.png](devtools_service_dfplayer_de.png)

Ein Klick auf den "Dienst ausführen" Button und die nächste MP3-Datei auf der SD-Karte sollte ertönen.

## Automatisierung in ESPHome

Ich möchte hier dieselbe Funktionalität wie mit dem Standalone-Programm erreichen. Ziel war ja, zusätzlich über Homeassistant die Grenzwerte einstellbar zu machen und die Bodenfeuchte grafisch darzustellen.

Die Logik soll weiterhin im Mikrocontroller liegen und nicht über Homeassistant-Automatisierung gelöst werden - ansonsten funktioniert es nicht mehr, wenn z.B. das Wlan unterbrochen wird.

Hierfür brauche ich erstmal Variablen, die ich über Homeassistant einstellen kann und vom Mikrocontroller benutzt werden können.

### Variablen in Homeassistant

Um die Variablen in Homeassistant zu steuern, brauche ich sogenannte "Helfer". Die finden sich unter "Einstellungen" -> "Geräte & Dienste" im rechten Tab "Helfer".

Für den sprechenden Pflanzenmonitor werden zwei Helfer benötigt. Diese können über die Schaltfläche "Helfer erstellen" angelegt werden. Als Typ wird jeweils "Nummer" ausgewählt und Name/Symbol wie folgt gewählt:
- `pflanzenmonitor_alert_interval` mit Symbol `mdi:timer`
- `pflanzenmonitor_minimum_moisture_percent` mit Symbol `mdi:water-percent`

Symbol und Anzeigemodus kann natürlich individuell angepasst werden.

Ich plane diese Variablen global für alle Pflanzenmonitore zu nutzen. Ansonsten müsste ich für jeden ein eigenes Variablen-Paar mit eindeutigem Namen anlegen.

### Variablen in ESPHome

In der Konfiguration unter ESPHome müssen die Variablen ebenfalls angelegt werden, dafür habe ich folgendes hinzugefügt:

```yaml
globals:
  - id: alert_interval
    type: int
    restore_value: yes
    initial_value: '10'
  - id: minimum_moisture_percent
    type: int
    restore_value: yes
    initial_value: '60'
```

Diese sind noch nicht mit den Homeassistant-Helfern verknüpft, dafür muss ich folgendes unter `sensor` ergänzen:

```yaml
sensor:
  # ...
  - platform: homeassistant
    name: Alert Interval
    entity_id: input_number.pflanzenmonitor_alert_interval
    on_value:
      then:
        - lambda: 'id(alert_interval) = x;'
  - platform: homeassistant
    name: Minimum Moisture Percentage
    entity_id: input_number.pflanzenmonitor_minimum_moisture_percent
    on_value:
      then:
        - lambda: 'id(minimum_moisture_percent) = x;'
```

Die Namen der Variablen sind die Entity-ID aus Homeassistant. Im Zweifel dort nachsehen, wie sie genau heißen.

### Script für Sound-Ausgabe

Um den Sound innerhalb eines Triggers auszugeben, reicht es, `dfplayer.play_next` auszuführen. Ich möchte aber, dass das nur alle x Sekunden geschieht, abhängig von der eben angelegten Variable. Deshalb brauche ich ein Script, das dann im Trigger gestartet werden kann.

So sieht es dann in der Konfiguration aus:

```yaml
script:
  - id: play_alert
    then:
      - logger.log: "Too dry - alerting!"
      - dfplayer.play_next:
      - delay: !lambda 'return id(alert_interval) * 1000;'
```

### Trigger hinzufügen

Jetzt ist alles bereit, um das Script abhängig der Zustände auch auszuführen. Ich habe mich dafür entschieden, das im `on_loop`-Trigger zu erledigen. Dafür habe ich am Anfang der Konfiguration folgendes ergänzt, ähnlich wie das `on_boot` für die Lautstärke des DFPlayer:

```yaml
  on_loop:
    then:
      - if:
          condition:
            and:
              - binary_sensor.is_on: motion1
              - lambda: 'return id(moisture1).state < id(minimum_moisture_percent);'
              - not:
                  script.is_running: play_alert
          then:
            - script.execute: play_alert
```

Übersetzt bedeutet das, sobald eine Bewegung erkannt wurde, die Feuchtigkeit zu gering ist und das Script nicht läuft, wird das Script gestartet. Das Script führt `dfplayer.play_next` aus und wartet x Sekunden. Während es wartet wird es also nicht erneut gestartet. Erst nachdem der Delay beendet wurde, wird es erneut ausgeführt, sofern noch alle anderen Bedingungen erfüllt sind.

### Testen

Für die Pflanzenmonitore habe ich eine neue Ansicht auf der Homeassistant-Seite angelegt und eine Gruppe mit den Variablen, sowie ein grafische Karte für die Sensor-Werte hinzugefügt:

![dashboard_one_sensor.png](dashboard_one_sensor.png)

Zum Testen kann ich jetzt den Sensor aus der Erde nehmen, oder warten bis die Erde trocken genug ist. Oder ich ändere den Vorgabewert mit dem Regler auf z.B. 100%. Sobald ich mich dann in den Bereich des Bewegungssensors begebe, ertönt wie gewünscht der Sound.

### Komplette Konfiguration

Ich habe die Konfigurationsdatei hier hinzugefügt: [pflanzenmonitor.yaml](pflanzenmonitor.yaml)

Da ich noch nicht ganz damit fertig bin, kann sie sich mit einem der künftigen Commits noch ändern.

## Mehrere Mikrokontroller mit derselben Konfiguration

Ich möchte jetzt mehr als einen Pflanzenmonitor aufstellen. Die einfachste Lösung dafür wäre, ein weiteres Gerät unter ESPHome zu erstellen und den Inhalt der YAML-Datei zu kopieren, mit angepasstem Gerätenamen und Schlüsseln. Das ist aber auf Dauer eine schlechte Lösung. Ich möchte es aber lieber als Template haben, damit wenn ich etwas ändern muss, das an zentraler Stelle tun kann.

Die Konfiguration entsprechend anzupassen ist also der nächste Schritt.

### Include-Datei

Der Inhalt der Konfiguration, der für alle Pflanzenmonitore benutzt werden soll, wird in eine eigene YAML-Datei ausgelagert, mit Variablen an entsprechenden Stellen.

Als Parameter benötige ich:
- Name (Name des Geräts)
- Friendly Name (dargestellter Name)
- Kalibrierungswerte (Luft- und Wasser-Wert)

Um die Bodenfeuchtesensoren zu kalibrieren, definiere ich einen zusätzlichen, internen Sensor mit den Rohwerten. Diese werden dann nur im Log angezeigt und ich kann sie nach erstmaligem Flashen auslesen und anpassen:

```yaml
sensor:
  # ...
  - platform: adc
    pin: A0
    name: "Soil Moisture Raw"
    id: moisture1_raw
    accuracy_decimals: 4
    update_interval: 10s
    unit_of_measurement: "V"
    internal: true
```

Die Konfiguration lege ich über das File Editor Addon unter `/config/esphome/includes/pflanzenmonitor_base.yaml` ab, hier die komplette Datei: [pflanzenmonitor_base.yaml](pflanzenmonitor_base.yaml).

### Include-Datei nutzen

Um einen neuen Pflanzenmonitor einzubinden, klicke ich wieder unter ESPHome auf "New Device", vergebe einen Namen, z.B. "Pflanzenmonitor1", wähle den Gerätetyp "ESP8266" aus und editiere dann die Konfiguration.

Der Abschnitt `esphome` wird in `substitutions` umbenannt und die beiden Werte für die Kalibrierung ergänzt:

```yaml
substitutions:
  name: "pflanzenmonitor1"
  friendly_name: Pflanzenmonitor1
  air_value: '0.760'
  water_value: '0.280'
```

Der Rest bleibt wie es automatisch angelegt wurde und am Ende wird das Include angehängt:

```yaml
<<: !include includes/pflanzenmonitor_base.yaml
```

Damit ist es fertig. Nach dem ersten Upload wird im Log alle 10 Sekunden der Wert des Bodenfeuchtesensors ausgegeben. Für die Kalibrierung halte ich ihn daher an der Luft und tauche ihn anschließend in Wasser. Die beiden Werte werden dann bei der Konfiguration des Geräts angepasst und erneut Hochgeladen.

Dashboard mit zwei Pflanzenmonitoren:

![dashboard_two_sensors.png](dashboard_two_sensors.png)
